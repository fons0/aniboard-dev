`import Ember from 'ember'`
`import InfinitePaginatedControllerMixin from 'aniboard/mixins/infinite-paginated-controller'`

module 'InfinitePaginatedControllerMixin'

# Replace this with your real tests.
test 'it works', ->
  InfinitePaginatedControllerObject = Ember.Object.extend InfinitePaginatedControllerMixin
  subject = InfinitePaginatedControllerObject.create()
  ok subject
