`import { test, moduleForModel } from 'ember-qunit'`

moduleForModel 'user-registration', 'UserRegistration', {
  # Specify the other units that are required for this test.
  needs: [
    'service:validations'
    'ember-validations@validator:base'
    'ember-validations@validator:local/presence'
    'ember-validations@validator:local/format'
  ]
}

test 'it exists', ->
  expect 0
  # model = @subject()
  # # store = @store()
  # ok !!model
