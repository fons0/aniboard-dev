`import { test, moduleForModel } from 'ember-qunit'`

moduleForModel 'user-show', 'UserShow', {
  # Specify the other units that are required for this test.
  needs: [
    'model:show-source'
    'model:airing-datetime'
    'model:show'
    'model:source'
  ]
}

test 'it exists', ->
  model = @subject()
  # store = @store()
  ok !!model
