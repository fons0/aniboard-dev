`import Configuration from 'simple-auth/configuration'`

testHelpers = (->
  Ember.Test.registerAsyncHelper 'login', (app, credentials) ->
    session = app.__container__.lookup Configuration.session
    session.authenticate 'authenticator:aniboard', credentials
    wait()
)()

`export default testHelpers`
