ClockInitializer =
  name: 'clock-service'
  initialize: (container, app) ->
    app.inject 'route:user-shows', 'clock', 'service:clock'
    app.inject 'controller:user-shows/show', 'clock', 'service:clock'

`export default ClockInitializer`
