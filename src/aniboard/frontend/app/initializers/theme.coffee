`import Ember from 'ember'`
`import {THEMES} from '../structures/settings'`

# theme loader
loadTheme = (theme) ->
  # get the theme on the settings on default and resolve the url
  url = THEMES[theme]

  new Ember.RSVP.Promise (resolve, reject) ->
    Ember.$.get url, ->
      # apply the new theme
      Ember.$('link[title="theme"]').attr 'href', url
      resolve()

# setups the initial theme and tracks the current set theme
setupThemeTracking = (settings) ->

# Takes two parameters: container and app
initialize = (container, app) ->
  # load the settings and determine if it's ready to get the theme from
  settings = container.lookup 'settings:main'
  settled = settings.get 'isSettled'

  # stop the app loading for a sec
  app.deferReadiness()

  settings.catch(->
    # do nothing, this is managed by the settings initializer
  ).finally ->
    # get the current theme
    currentTheme = settings.get('theme') or 'default'

    # add an observer to change the theme when it _changes_
    settings.addObserver 'theme', ->
      theme = settings.get 'theme'

      if theme? and theme isnt currentTheme
        currentTheme = theme
        loadTheme currentTheme

    # load the initial theme
    loadTheme(currentTheme).then -> app.advanceReadiness()

ThemeInitializer =
  name: 'theme'
  after: 'settings'
  initialize: initialize

`export {initialize}`
`export default ThemeInitializer`
