`import AniboardAuthenticator from '../authenticators/aniboard'`

# Takes two parameters: container and app
initialize = (container) ->
  container.register 'authenticator:aniboard', AniboardAuthenticator

AuthenticationInitializer =
  name: 'authentication'
  before: 'simple-auth'
  initialize: initialize

`export {initialize}`
`export default AuthenticationInitializer`
