`import Ember from 'ember'`
`import ENV from '../config/environment'`

# Takes two parameters: container and app
initialize = () ->
  if ENV.environment is 'development'
    Ember.onerror = (error) ->
      if Ember.typeOf(error) is 'object'
        message = error?.message or 'No error message'
        Ember.Logger.error("RSVP Error: #{message}")
        Ember.Logger.error(error?.stack)
        Ember.Logger.error(error?.object)
      else
        Ember.Logger.error 'RSVP Error', error

OnerrorInitializer =
  name: 'onerror'
  initialize: initialize

`export {initialize}`
`export default OnerrorInitializer`
