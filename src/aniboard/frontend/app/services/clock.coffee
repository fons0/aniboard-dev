`import Ember from 'ember'`

ClockService = Ember.Object.extend
  time: Ember.computed.readOnly '_time'
  _previousTime: null
  _time: moment().startOf 'minute'

  dayChanged: Ember.computed ->
    time = @get '_time'
    previousTime = @get '_previousTime'

    time.get('weekday') != previousTime.get('weekday')

  _updateTime: Ember.on 'init', Ember.observer('_time', ->
    time = moment()
    nextInterval = moment().add(1, 'minutes').startOf 'minute'
    untilNext = nextInterval.diff time

    # we use setTimeout because run.later causes problems with testing
    setTimeout (Ember.run.bind @, ->
      # since we're using a timeout, this can be called once the instance
      # was destroyed

      if !@get('isDestroyed')
        previousTime = @get '_time'
        time = moment().startOf 'minute'

        @setProperties
          _time: time
          _previousTime: previousTime

        # do it by hand
        @notifyPropertyChange 'dayChanged'
    ), untilNext
  )

`export default ClockService`
