`import Ember from 'ember'`

TextMarqueeComponent = Ember.Component.extend
  textElement: ''
  speed: 50

  mouseEnter: ->
    speed = @get 'speed'
    $parent = @$ @get 'textElement'
    $child = $parent.children().first()
    $child.css
      width: 'inherit'
      'text-overflow': 'inherit'

    if $parent.width() < $child.width()
      diff = Math.ceil $parent.width() - $child.width()
      dur = -diff / speed * 1000

      $child.transition
        'text-indent': diff
        duration: dur
        easing: 'linear'

  mouseLeave: ->
    $parent = @$ @get 'textElement'
    $child = $parent.children().first()

    $child.stop().css
      transition: ''
      'text-indent' : ''
      'text-overflow': 'ellipsis'
      width: ''

`export default TextMarqueeComponent`
