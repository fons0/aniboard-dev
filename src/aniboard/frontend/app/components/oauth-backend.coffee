`import Ember from 'ember'`

OauthBackendComponent = Ember.Component.extend
  init: ->
    @_super()

    # check the url for the code parameter
    code = $.url().param 'code'

    if code
      @sendAction 'action', code: code

`export default OauthBackendComponent`
