`import DS from 'ember-data'`
`import EmberValidations from 'ember-validations'`

UserShowComment = DS.Model.extend EmberValidations.Mixin,
  userShow: DS.belongsTo 'user-show', async: true
  user: DS.belongsTo 'user'
  episode: DS.attr 'number'
  modified: DS.attr 'date'
  liked: DS.attr 'nullBoolean'
  comment: DS.attr 'string'

  neutral: Ember.computed.equal 'liked', null
  disliked: Ember.computed.equal 'liked', false

  modifiedMoment: Ember.computed 'modified', ->
    moment @get 'modified'

  validations:
    liked:
      presence: true
    comment:
      length:
        allowBlank: true
        maximum: 140

`export default UserShowComment`
