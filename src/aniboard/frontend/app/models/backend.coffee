`import Ember from 'ember'`
`import DS from 'ember-data'`

Backend = DS.Model.extend
  backendName: DS.attr 'string'
  name: DS.attr 'string'
  description: DS.attr 'string'
  isConnected: DS.attr 'boolean'
  logoUrl: DS.attr 'string'
  authUrl: DS.attr 'string'

  authType: Ember.computed 'authUrl', ->
    authUrl = @get 'authUrl'

    if authUrl
      'oauth'
    else
      'simpleauth'

  isConnecting: false
  isDisconnecting: false

  baseUrl: Ember.computed ->
    adapter = @store.adapterFor Backend
    adapter.buildURL 'backend', @id, @

  connectUrl: Ember.computed 'baseUrl', ->
    "#{@get 'baseUrl'}connect/"

  disconnectUrl: Ember.computed 'baseUrl', ->
    "#{@get 'baseUrl'}disconnect/"

  connect: (credentials) ->
    new Ember.RSVP.Promise (resolve, reject) =>
      @set 'isConnecting', true

      $.ajax(
        type: 'POST'
        url: @get 'connectUrl'
        contentType: 'application/json'
        data: JSON.stringify credentials: credentials
      ).done((data) =>
        @store.push 'backend', @store.normalize('backend', data)

        if @get 'isConnected'
          resolve()
        else
          reject()
      ).fail(->
        reject()
      ).always =>
        @set 'isConnecting', false

  disconnect: ->
    new Ember.RSVP.Promise (resolve, reject) =>
      @set 'isDisconnecting', true

      $.ajax(
        type: 'POST'
        url: @get 'disconnectUrl'
        contentType: 'application/json'
      ).done((data) =>
          @store.push 'backend', @store.normalize('backend', data)

          if not @get 'isConnected'
            resolve()
          else
            reject()
      ).fail(->
        reject()
      ).always =>
        @set 'isDisconnecting', false

`export default Backend`
