`import DS from 'ember-data'`

defaultCoverImage = '/images/default_image.png'

Show = DS.Model.extend
  name: DS.attr 'string'
  slug: DS.attr 'string'
  airing: DS.attr 'boolean'
  numberEpisodes: DS.attr 'number'
  coverImageUrl: DS.attr 'string'
  showSources: DS.hasMany 'show-source', async: true
  score: DS.attr 'number'
  addedDate: DS.attr 'date'

  addedMoment: Ember.computed 'addedDate', ->
    moment @get 'addedDate'

  isNew: Ember.computed 'addedMoment', ->
    delta = moment() - @get('addedMoment')

    return delta < moment.duration(1, 'month')

  coverImage: Ember.computed 'coverImageUrl', ->
    url = @get 'coverImageUrl'

    if not url?
      url = defaultCoverImage

    return url

  hasScore: Ember.computed.notEmpty 'score'

`export default Show`
