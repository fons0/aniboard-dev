`import DS from 'ember-data'`

NullBooleanTransform = DS.BooleanTransform.extend
  deserialize: (serialized) ->
    if serialized?
      serialized = @_super serialized

    serialized

  serialize: (deserialized) ->
    if deserialized?
      deserialized = @_super deserialized

    deserialized

`export default NullBooleanTransform`
