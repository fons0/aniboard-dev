`import Ember from 'ember'`

CommentsEditController = Ember.Controller.extend
  actions:
    closeDialog: ->
      model = @get 'model'

      if model.get('isDirty')
        model.rollback()

      # bubble action
      true

    comment: (liked) ->
      # if liked has no value, asume neutral
      liked = null unless liked?

      likedChanged = @get('model.liked') isnt liked
      @set 'model.liked', liked

      if not @get 'model.isSaving'
        model = @get 'model'
        name = model.get 'userShow.name'
        episode = model.get 'episode'

        # try to save
        @set 'hermes.info', "Commenting on episode #{episode} of
          **#{name}**..."

        model.save().then(=>
          @set 'hermes.success', "Successfully commented on episode #{episode}
            of **#{name}**."

          if likedChanged
            # trigger a reload of the user show, since the score may have
            # changed (we have to use .content because it's an async property)
            model.get('userShow.content').reload().then =>
              # close the dialog
              @send 'closeDialog'
          else
              # close the dialog
              @send 'closeDialog'
        ).catch =>
          @set 'hermes.error', "Failed to comment on episode #{episode} of
            **#{name}**."

`export default CommentsEditController`
