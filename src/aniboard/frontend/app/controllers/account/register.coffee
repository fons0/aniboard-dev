`import Ember from 'ember'`
`import EmberValidations from 'ember-validations'`

AccountRegisterController = Ember.Controller.extend EmberValidations.Mixin,
  email: Ember.computed.alias 'model.email'
  username: Ember.computed.alias 'model.username'
  password: Ember.computed.alias 'model.password'

  validations:
    email:
      presence: true
      format:
        with: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i
        message: 'must be a valid email'
      inline: EmberValidations.validator ->
        errors = @model.get('model.errors')

        if errors?
          emailErrors = errors.errorsFor 'email'

          if emailErrors.length
            emailErrors.get 'firstObject.message'
    username:
      presence: true
      inline: EmberValidations.validator ->
        errors = @model.get('model.errors')

        if errors?
          usernameErrors = errors.errorsFor 'username'

          if usernameErrors.length
            usernameErrors.get 'firstObject.message'
    password:
      presence: true

  actions:
    register: ->
      if not @get 'model.isSaving'
        model = @get 'model'

        # try to save
        @set 'hermes.info','Registering...'

        model.save().then( =>
          @set 'hermes.success', 'Succesfully registered!'

          # send ga event
          @get('ga').send 'event', 'account', 'register'

          # show the register-done message
          @transitionToRoute 'account.register.done'
        ).catch =>
          @set 'hermes.error', 'Failed to register.'

          # revalidate and show errors
          @validate().catch =>
            @toggleProperty 'showErrors'

`export default AccountRegisterController`
