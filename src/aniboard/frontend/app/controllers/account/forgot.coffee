`import Ember from 'ember'`

AccountForgotController = Ember.Controller.extend
  actions:
    forgot: ->
      if not @get 'model.isSaving'
        model = @get 'model'

        # try to save
        @set 'hermes.info','Sending recovery request...'

        model.save().then( =>
          @set 'hermes.success', 'Succesfully sent!'

          # close the dialog
          @send 'closeDialog'
        ).catch =>
          @set 'hermes.error', 'Failed to send'

`export default AccountForgotController`
