`import Ember from 'ember'`
`import InfinitePaginatedControllerMixin from '../mixins/infinite-paginated-controller'`

NoticesController = Ember.ArrayController.extend InfinitePaginatedControllerMixin,
  days: Ember.computed 'model', ->
    Ember.ArrayProxy.create
      content: []
      sortProperties: ['unixTime']

  populateDays: Ember.observer 'model', ->
    @forEach @assignNotice, @

  contentArrayDidChange: (model, start, removeCount, addCount) ->
    if addCount
      model.slice(start, start + addCount).forEach @assignNotice, @

  assignNotice: (notice) ->
    days = @get 'days'
    date = notice.get('moment').startOf 'day'
    unixTime = date.unix()
    day = days.findBy 'unixTime', unixTime

    if not day?
      day =
        unixTime: unixTime
        date: date
        notices: []
      days.addObject day

    day.notices.addObject notice

`export default NoticesController`
