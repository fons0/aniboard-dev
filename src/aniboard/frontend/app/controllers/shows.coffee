`import Ember from 'ember'`
`import DebouncedPropertiesMixin from 'ember-debounced-properties/mixin'`

ShowsController = Ember.ArrayController.extend DebouncedPropertiesMixin,
  shows: []
  sortProperties: ['name']
  searchTerm: ''
  debouncedProperties: ['searchTerm']

  init: ->
    @_super()

    @eventHub.on 'userShow:added', @, @listeners.showAdded

  searchRegex: Ember.computed 'searchTerm', ->
    searchTerm = @get 'searchTerm'
    new RegExp(searchTerm, 'gi') unless searchTerm is ''

  _filterShows: ->
    searchRegex = @get 'searchRegex'
    shows = @get 'shows'

    if not searchRegex
      shows.setObjects @get 'model'
    else
      shows.setObjects @filter(
        (show) ->
          show.get('name').match(searchRegex)?
        ,
        @
      )

  filterShows: Ember.observer 'model', 'debouncedSearchTerm', ->
    shows = @get 'shows'

    if shows.length
      shows.clear()
      Ember.run.later @, @_filterShows, 500
    else
      @_filterShows()

  listeners:
    showAdded: (userShow) ->
      # remove the show from the list and the filtered list
      show = userShow.get 'showSource.show'
      @removeObject show
      @get('shows').removeObject show

`export default ShowsController`
