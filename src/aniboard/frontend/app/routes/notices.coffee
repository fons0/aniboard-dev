`import Ember from 'ember'`
`import {InfinitePaginatedArray} from '../structures/paginated-array'`
`import AuthenticatedRouteMixin from 'simple-auth/mixins/authenticated-route-mixin'`

NoticesRoute = Ember.Route.extend AuthenticatedRouteMixin,
  titleToken: 'Notices'

  model: ->
    InfinitePaginatedArray.create
      store: @store
      modelName: 'notice'

`export default NoticesRoute`
