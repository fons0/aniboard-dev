`import Ember from 'ember'`
`import DialogRouteMixin from '../../mixins/dialog/route'`
`import AuthenticatedRouteMixin from 'simple-auth/mixins/authenticated-route-mixin'`

CommentsEditRoute = Ember.Route.extend DialogRouteMixin, AuthenticatedRouteMixin,
  titleToken: 'Comment'

  model: (params) ->
    userShow = @modelFor 'userShow'
    commentsParams = @paramsFor 'comments'

    username = @session.get 'username'
    slug = userShow.get 'slug'
    episode = parseInt commentsParams.episode

    new Ember.RSVP.Promise (resolve, reject) =>
      @store.find('userShowComment'
        slug: slug
        episode: episode
        user: username
      ).then((result) =>
        if result and result.get('length') is 1
          comment = result.get 'firstObject'
        else
          comment = @store.createRecord 'userShowComment',
            userShow: userShow
            episode: episode

        resolve comment
      ).catch (error) ->
        reject error

  actions:
    closeDialog: ->
      # ask for rating if needed
      askRating = @get 'settings.askRating'
      userShow = @modelFor 'userShow'
      askShowRating = userShow.get 'askRating'

      if askRating and askShowRating
        @transitionTo 'userShow.rate', userShow
        return false
      else
        # bubble to parent route
        return true

`export default CommentsEditRoute`
