`import Ember from 'ember'`
`import DialogRouteMixin from '../../mixins/dialog/route'`
`import AuthenticatedRouteMixin from 'simple-auth/mixins/authenticated-route-mixin'`
`import {InfinitePaginatedArray} from '../../structures/paginated-array'`

CommentsIndexRoute = Ember.Route.extend DialogRouteMixin, AuthenticatedRouteMixin,
  titleToken: 'Comments'

  model: (params) ->
    userShow = @modelFor 'userShow'
    commentsParams = @paramsFor 'comments'

    slug = userShow.get 'slug'
    episode = parseInt commentsParams.episode

    new Ember.RSVP.Promise (resolve, reject) =>
      @store.find('showEpisode'
        slug: slug
        episode: episode
      ).then((result) =>
        if result and result.get('length') is 1
          showEpisode = result.get 'firstObject'
        else
          showEpisode = @store.createRecord 'showEpisode',
            show: userShow.get 'show'
            episode: episode

        comments = InfinitePaginatedArray.create
          store: @store
          modelName: 'userShowComment'
          perPage: 3
          fetchOnInit: false
          opts:
            'slug': showEpisode.get 'slug'
            'episode': showEpisode.get 'episode'

        resolve
          showEpisode: showEpisode
          userShow: userShow
          comments: comments
      ).catch (error) ->
        reject error

  setupController: (controller, model) ->
    controller.set 'model', model.comments
    controller.set 'showEpisode', model.showEpisode
    controller.set 'userShow', model.userShow

`export default CommentsIndexRoute`
