`import Ember from 'ember'`
`import DialogRouteMixin from '../../mixins/dialog/route'`
`import UnauthenticatedRouteMixin from 'simple-auth/mixins/unauthenticated-route-mixin'`

AccountForgotRoute = Ember.Route.extend DialogRouteMixin, UnauthenticatedRouteMixin,
  titleToken: 'Forgot Password'

  model: (params) ->
    @store.createRecord 'passwordForgot'

`export default AccountForgotRoute`
