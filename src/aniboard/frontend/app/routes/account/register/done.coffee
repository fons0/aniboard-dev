`import Ember from 'ember'`
`import DialogRouteMixin from 'aniboard/mixins/dialog/route'`
`import UnauthenticatedRouteMixin from 'simple-auth/mixins/unauthenticated-route-mixin'`

AccountRegisterDoneRoute = Ember.Route.extend DialogRouteMixin, UnauthenticatedRouteMixin,
  titleToken: 'Done'

`export default AccountRegisterDoneRoute`
