`import Ember from 'ember'`
`import DialogRouteMixin from '../../mixins/dialog/route'`
`import AuthenticatedRouteMixin from 'simple-auth/mixins/authenticated-route-mixin'`

BackendConnectRoute = Ember.Route.extend DialogRouteMixin, AuthenticatedRouteMixin,
  titleToken: 'Connect'

  afterModel: (model, transition) ->
    if model.get 'isConnected'
      @transitionTo 'backend.disconnect', model

`export default BackendConnectRoute`
