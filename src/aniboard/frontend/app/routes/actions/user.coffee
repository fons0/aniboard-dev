`import Ember from 'ember'`

ActionsUserRoute = Ember.Route.extend
  model: (params) ->
    @set 'titleToken', params.user
    @store.find 'userShowAction',
      user: params.user
      since: moment().startOf('day').subtract(6, 'days').format()

  setupController: (controller, model) ->
    @_super controller, model
    controller.set 'user', @get 'titleToken'

`export default ActionsUserRoute`
