`import Ember from 'ember'`
`import DialogRouteMixin from '../../mixins/dialog/route'`
`import AuthenticatedRouteMixin from 'simple-auth/mixins/authenticated-route-mixin'`

UserShowEditRoute = Ember.Route.extend DialogRouteMixin, AuthenticatedRouteMixin,
  titleToken: 'Edit'

  setupController: (controller, model) ->
    # setup controller
    @_super controller, model
    controller.set 'editable.lastEpisodeWatched', model.get('lastEpisodeWatched')
    controller.set 'editable.showSource', model.get('showSource')

`export default UserShowEditRoute`
