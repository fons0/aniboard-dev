`import Ember from 'ember'`
`import ApplicationRouteMixin from 'simple-auth/mixins/application-route-mixin'`
`import generateTitle from '../utils/generate-title'`

ApplicationRoute = Ember.Route.extend ApplicationRouteMixin,
  title: (tokens) ->
    tokens.reverse()
    generateTitle tokens

  actions:
    sessionAuthenticationFailed: (error) ->
      @set 'hermes.error', error.message

    sessionAuthenticationSucceeded: ->
      @set 'hermes.success', 'Authentication succeeded!'
      @refresh()

    openDialog: (name, content) ->
      controller = @controllerFor name

      if content?
        controller.set 'model', content

      @render name,
        outlet: 'dialog'
        into: 'application'

    closeDialog: ->
      @disconnectOutlet
        outlet: 'dialog'
        parentView: 'application'

    openFeedbackDialog: ->
      feedback = @store.createRecord 'feedback'
      @send 'openDialog', 'feedback', feedback

`export default ApplicationRoute`
