`import Ember from 'ember'`
`import transitionEnd from '../utils/transition-end'`

FadeViewMixin = Ember.Mixin.create
  classNameBindings: ['hide']
  hide: true

  animateIn: (done) ->
    @set 'hide', false
    $el = @$()

    if $el
      transitionEnd $el, 500, done
    else
      done()

  animateOut: (done) ->
    @set 'hide', true
    $el = @$()

    if $el
      transitionEnd $el, 500, done
    else
      done()

`export default FadeViewMixin`
