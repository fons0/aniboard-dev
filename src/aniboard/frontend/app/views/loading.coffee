`import Ember from 'ember'`
`import FadeViewMixin from '../mixins/fade-view'`

LoadingView = Ember.View.extend FadeViewMixin,
  classNames: ['loading']

`export default LoadingView`
