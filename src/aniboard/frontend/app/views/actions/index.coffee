`import Ember from 'ember'`
`import FadeViewMixin from '../../mixins/fade-view'`

ActionsIndexView = Ember.View.extend FadeViewMixin,
  classNames: 'stalk'

`export default ActionsIndexView`
