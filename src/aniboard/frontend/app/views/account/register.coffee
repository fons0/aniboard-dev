`import Ember from 'ember'`
`import DialogViewMixin from '../../mixins/dialog/view'`

AccountRegisterView = Ember.View.extend DialogViewMixin,
  classNames: ['register']

`export default AccountRegisterView`
