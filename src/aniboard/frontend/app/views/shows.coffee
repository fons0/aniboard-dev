`import Ember from 'ember'`
`import FadeViewMixin from '../mixins/fade-view'`

ShowsView = Ember.View.extend FadeViewMixin,
  classNames: ['add_show']

`export default ShowsView`
