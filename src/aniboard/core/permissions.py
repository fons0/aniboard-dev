from rest_framework import permissions


class IsOwner(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        user_field = getattr(view, 'user_field', 'user')

        for bit in user_field.split('.'):
            obj = getattr(obj, bit)

        return obj == request.user
