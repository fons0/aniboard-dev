from __future__ import absolute_import

from django.utils import text, timezone
import requests
import json
from aniboard.core import models
from aniboard.core.importer import base


class JSONShowsParser(base.BaseParser):
    def parse_entries(self, import_text):
        return json.loads(import_text)

    def preprocess_entry(self, entry):
        try:
            entry['slug'] = text.slugify(entry['title_ro'])
        except KeyError:
            raise base.PreprocessException(
                'Doesn\'t have a title, which is required'
            )

        # we have to download the image link
        if 'image' in entry:
            try:
                response = requests.get(entry['image'], stream=True)
                response.raw.decode_content
                response.raise_for_status()
                entry['img_content'] = response
            except Exception as ex:
                raise base.PreprocessException(
                    'The image couldn\'t be retrieved: {}'.format(ex)
                )

        if 'sources' in entry:
            for source_info in entry['sources']:
                # obtain the sources from the name
                try:
                    source = models.Source.objects.filter(
                        name_ja=source_info['name']
                    ).first()

                    if source is None:
                        source = models.Source.objects.create(
                            name=source_info['name'],
                            name_ja=source_info['name']
                        )

                    source_info['is_premiere'] = source_info.get(
                        'premiere', False
                    )
                    source_info['source'] = source
                    if 'date' in source_info:
                        source_info['date'] = timezone.datetime.strptime(
                            source_info['date'], '%Y-%m-%d'
                        )
                    if 'time' in source_info:
                        source_info['time'] = timezone.datetime.strptime(
                            source_info['time'], '%H:%M'
                        ).time()
                except ValueError:
                    raise base.PreprocessException(
                        'The date/time couldn\'t be parsed'
                    )

    def _update_image(self, entry, show):
        """ Updates the image for a show. """
        self.update_image(
            entry['image'],
            entry['img_content'].raw.read(),
            show
        )

    def _update_api_datas(self, entry, show):
        """ Creates and/or updates the related api data. """
        backends_keys = [
            ('id_mal', 'mal'), ('id_hb', 'hummingbird'),
            ('id_anilist', 'anilist')
        ]
        backends = {}

        for api_key, backend in backends_keys:
            if api_key in entry:
                backends[backend] = entry[api_key]

        self.update_api_datas(backends, show)

    def _update_show_sources(self, entry, show):
        if 'sources' in entry:
            for source in entry['sources']:
                self.update_show_source(source, show)

    def create_show(self, entry):
        # initial show
        show = models.Show.objects.create(
            slug=entry['slug'],
            name=entry['title_ro'],
            name_ja=entry.get('title_ja', None),
            number_episodes=entry.get('eps', None),
            syoboi_id=entry.get('id_sb', None)
        )

        if 'image' in entry:
            # add the image if there's one
            self._update_image(entry, show)

        # show source
        self._update_show_sources(entry, show)

        # api datas
        self._update_api_datas(entry, show)

        return show

    def update_show(self, entry, show):
        # update base fields
        if 'title_ja' in entry:
            show.name_ja = entry['title_ja']

        if 'eps' in entry:
            show.number_episodes = entry['eps']

        if 'id_sb' in entry:
            show.syoboi_id = entry['id_sb']

        show.save()

        if 'image' in entry:
            # the image can be missing
            self._update_image(entry, show)

        # update api datas if needed
        self._update_api_datas(entry, show)

        # update show source
        self._update_show_sources(entry, show)

    def postprocess_entry(self, entry):
        if 'image' in entry and entry['img_content'] is not None:
            # we have to make sure the request is closed
            entry['img_content'].close()
