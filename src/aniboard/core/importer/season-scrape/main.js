var request = require('request');
var cheerio = require('cheerio');
var fs = require('fs');
var jf = require('jsonfile');
var moment = require('moment');
require('moment-timezone');
require('fuzzyset.js');
var _ = require('underscore');

var lcURL = 'https://www.livechart.me/';
var sbURL = 'http://cal.syoboi.jp/quarter/';
var crURL = 'http://www.crunchyroll.com/';
var funURL = 'http://www.funimation.com/videos/simulcasts';

var patch = function(post) {
  console.log('patching');
  
  var sb = jf.readFileSync('./syoboi');
  var lc = jf.readFileSync('./livechart');
  var patch = jf.readFileSync('./patch');
  
  if(post) {
    /*var merge = jf.readFileSync('./merged.json');
    for(var i in patch) {
      if(patch[i].id_mal) {
        console.log('patch for merge');
        for(var j in merge) {
          if(merge[j].id_mal == patch[i].id_mal) {
            for(var key in patch[i]) merge[j][key] = patch[i][key];
          }
        }
      }
    }
    fs.writeFileSync('./merged.json', JSON.stringify(merge,null,2));*/
    return;
  }
  
  for(var i in patch) {
    if(patch[i].id_mal) {
      console.log('patch for livechart');
      for(var j in lc) {
        if(lc[j].id_mal == patch[i].id_mal) {
          for(var key in patch[i]) lc[j][key] = patch[i][key];
        }
      }
    } else if(patch[i].id_sb) {
      console.log('patch for syoboi');
      for(var j in sb) {
        if(sb[j].id_sb == patch[i].id_sb) {
          //console.log('patch match syoboi');
          for(var k in patch[i].sources) {
            var pat = patch[i].sources[k];
            //console.log(pat);
            for(var l in sb[j].sources) {
              var syo = sb[j].sources[l];
              //console.log(syo);
              if(syo['name'] == pat['name']) {
                for(var key in pat) {
                  syo[key] = pat[key];
                }
                break;
              }
            }
          }
        }
      }
    }
  }
  
  fs.writeFileSync('./livechart', JSON.stringify(lc,null,2));
  fs.writeFileSync('./syoboi', JSON.stringify(sb,null,2));
}

var getDayNum = function(day) {
  switch(day) {
    case 'SUN':
      return 0;
      break;
    case 'MON':
      return 1;
      break;
    case 'TUE':
      return 2;
      break;
    case 'WED':
      return 3;
      break;
    case 'THU':
      return 4;
      break;
    case 'FRI':
      return 5;
      break;
    case 'SAT':
      return 6;
      break;
  }
}

var funScrape = function() {
  var url = funURL;
  var shows = jf.readFileSync('./merged.json');
  var items = [];
  var re_time = /(.+) Eastern/i;
  var re_slug = /shows\/([^\/]+)\//i;
  
  request({url:url}, function(err, res, body) {
    $ = cheerio.load(body);
    var vids = [];
    
    $('.column_2').eq(1).find('.item-title').each(function() {
      var slug = $(this).attr('href').match(re_slug)[1];
      
      vids.push(slug);
    });
    
    $('.simulcast_schedule .item-title').each(function() {
      var item = {};
      
      var slug = $(this).attr('href').match(re_slug)[1];
      var has_vid = (vids.indexOf(slug) != -1)? true : false;
      var time = $(this).next().text().match(re_time)[1];
      time = moment(time,'h:mm a').format('HH:mm');
      var day = $(this).closest('.item-cell').find('.day').text();
      
      var date = moment().tz('America/New_York').startOf('week').add(getDayNum(day),'days');//.format('YYYY-MM-DD')
      var diff = date.diff(moment().tz('America/New_York'),'days');
      var is_old = (diff < 0)? true : false;
      if(is_old && !has_vid) {
        date = date.add(1,'weeks').format('YYYY-MM-DD');
      } else {
        date = date.format('YYYY-MM-DD');
      };
      var datetime = moment.tz(date+' '+time,'America/New_York').utc().format('YYYY-MM-DD HH:mm');
      console.log(slug,time,day,datetime,diff,is_old,has_vid);
      
      date = datetime.split(' ')[0];
      time = datetime.split(' ')[1];
      
      item.id = slug;
      item.date = date;
      item.time = time;
      
      items.push(item);
    });
    addSource();
  });
  
  var addSource = function() {
    for(var i in shows) {
      var source = {
        "name": "FUNimation"
      }
      var show = shows[i];
      
      if(show.id_fun) {
        _.find(items, function(item) {
          if(item.id == show.id_fun) {
            source.date = item.date;
            source.time = item.time;
          }
        });
        show.sources.push(source);
      }
    }
    
    fs.writeFileSync('./merged.json', JSON.stringify(shows,null,2));
  }
}

var crScrape = function(cb) {
  var url = crURL + 'launchcalendar';
  var shows = jf.readFileSync('./merged.json');
  var events = [];
  var re_date = /},(?: )?"(.+)"/;
  var re_ep = /Episod(?:e|io)(?:s)? (\d*)/i;
  
  var cal = moment().add(3,'days');
  
  var params = {
    month: cal.format('M'),
    year: cal.format('YYYY')
  }
  console.log(params);
  
  request({url:url,params:params}, function(err, res, body) {
    $ = cheerio.load(body);
    //console.log(body);
    
    $('.calendar-month-date-event').each(function() {
      var event = {};
    
      var slug = $(this).attr('href').split('/')[1];
      //var time = $(this).find('.time')
      var datetime = $(this).next().text().match(re_date)[1];
      datetime = moment(datetime,'ddd, MMMM DD, h:mma').format('YYYY-MM-DD HH:mm');
      datetime = moment.tz(datetime,'America/Los_Angeles').utc().format('YYYY-MM-DD HH:mm');
      var date = datetime.split(' ')[0];
      var time = datetime.split(' ')[1];
      var ep = $(this).text().match(re_ep);
      if(ep) ep = ep[1]
      var is_gold = /\#FFFBDF/i.test($(this).attr('style'));
      //console.log(is_gold);
      
      //console.log(slug,ep,date,time);
      
      if(is_gold && ep == 1) {
        event.id = slug;
        event.date = date;
        event.time = time;
        
        events.push(event);
      }
    });
    
    console.log(events);
    addSource();
  });
  
  var addSource = function() {
    for(var i in shows) {
      var source = {
        "name": "Crunchyroll"
      }
      var show = shows[i];
      
      if(show.id_cr) {
        _.find(events, function(event) {
          if(event.id == show.id_cr) {
            source.date = event.date;
            source.time = event.time;
          }
        });
        show.sources.push(source);
      }
    }
    
    fs.writeFileSync('./merged.json', JSON.stringify(shows,null,2));
    if(cb) cb();
    funScrape();
  }
}

var lcScrape = function(specific, cb) {
  var url = (specific)? lcURL+specific+'/tv' : lcURL;
  /*var url;
  if(!specific) {
    request({url:lcURL}, function(err, res, body) {
      $ = cheerio.load(body);
      url = lcURL + $('.chart-nav > ul > li').last().children().first().attr('href');
      console.log(url);
      doRequest();
    });
  } else {
    url = lcURL+specific+'/all';
    doRequest();
  }*/
  
  //var doRequest = function() {
    request({url:url}, function(err, res, body) {
      $ = cheerio.load(body);

      var season = $('.chart-heading h1').text().split(' Anime')[0];
      //year = season.split(' ')[1];
      //season = season.split(' ')[0];

      console.log(season);

      var shows = [];

      $('.anime-card').each(function(index) {
        var $ = cheerio.load(this);

        var show = {};

        show.title_ro = $('.main-title').text();
        show.title_ja = $('.jp-title').text();
        var eps = $('.anime-episodes').text();
        eps = eps.split(' eps')[0];
        if(/\d+/.test(eps)) show.eps = parseInt(eps);

        var links = [
          '.mal-icon', '.anime-planet-icon', '.anidb-icon', '.hummingbird-icon',
          '.crunchyroll-icon', '.funimation-icon', '.daisuki-icon'
        ];
        show.sources = [];

        for(var i in links) {
          if($(links[i]).length) {
            var url = $(links[i]).attr('href');
            if(/myanimelist/i.test(url)) show.id_mal = url.split('anime/')[1];
            if(/anime-planet/i.test(url)) show.id_ap = url.split('anime/')[1];
            if(/anidb/i.test(url)) show.id_adb = url.split('.net/')[1];
            if(/hummingbird/i.test(url)) show.id_hb = url.split('anime/')[1];
            //if(/crunchyroll/i.test(url)) show.id_cr = url.split('.com/')[1];
            if(/crunchyroll/i.test(url)) show.sources.push({
              'name': 'Crunchyroll',
              'id': url.split('.com/')[1]
            });
            //if(/funimation/i.test(url)) show.id_fun = url.split('shows/')[1].split('/')[0];
            if(/funimation/i.test(url)) show.sources.push({
              'name': 'FUNimation',
              'id': url.split('shows/')[1].split('/')[0]
            });
            //if(/daisuki/i.test(url)) show.id_ds = url.split('detail/')[1].split('/')[0];
            if(/daisuki/i.test(url)) show.sources.push({
              'name': 'DAISUKI',
              'id': url.split('detail/')[1].split('/')[0]
            });
          }
        }

        shows.push(show);
      })

      fs.writeFileSync('./livechart', JSON.stringify(shows,null,2));
      if(cb) cb();
    })
  //}
}

var sbScrape = function(specific, cb) {
  var url = (specific)? sbURL+specific : sbURL;
  request({url:url}, function(err, res, body) {
    $ = cheerio.load(body);
    //console.log(body);

    var quarter = $('#main h1').text();
    console.log(quarter);

    var shows = [];
    $('.titlesDetail > li').each(function(index) {
      var $ = cheerio.load(this);
      var show = {};

      show.title_ja = $('.title').text().trim();
      id_sb = $('.title').attr('href');
      id_sb = id_sb.split('tid/')[1];
      show.id_sb = id_sb;
      //console.log(show.title_ja,show.id_sb);

      if($('.progs').length) {
        show.sources = [];
        $('.progs li').each(function(ind) {
          var $ = cheerio.load(this);
          var source = {};
          
          if(ind === 0) source.premiere = true; 
          source.name = $('.ChName').text(); //console.log(source.name);
          var datetime = $('.StTime').text();
          var date = datetime.split(' (')[0];
          var time = datetime.split(') ')[1];
          time = time.replace(' ','0');
          //console.log(date,time);
          datetime = toUTC(date,time);
          //console.log(datetime);
          source.date = datetime.split(' ')[0];
          source.time = datetime.split(' ')[1];

          show.sources.push(source); //console.log(show.sources);
        })
      }
      shows.push(show);
    })

    fs.writeFileSync('./syoboi', JSON.stringify(shows,null,2));
    if(cb) cb();
  });
}

var toUTC = function(date,time) {
  var timec = parseInt(time.replace(':',''), 10);
  if(timec >= 2400) {
    time = (timec - 2400).toString();
    while(time.length < 4) {
      time = 0+time;
    }
    time = time.substr(0,2) + ':' + time.substr(2);
    date = moment(date).add(1,'d').format('YYYY-MM-DD');
  }
  var datetime = date+' '+time+':00+09:00';
  var newDatetime = moment(datetime).utc().format('YYYY-MM-DD HH:mm');
  return newDatetime;
}

var merge = function(cb) {
  patch();

  console.log('merging');
  var lc = jf.readFileSync('./livechart');
  var sb = jf.readFileSync('./syoboi');

  var a0 = FuzzySet();
  var a = FuzzySet();
  var b = FuzzySet();
  var c = FuzzySet();
  var d = FuzzySet();
  var titles0 = {};
  var titles = {};
  var titlesb = {};
  var titlesc = {};
  var titlesd = {};
  var merged = [];
  var indexes = [];
  var matched = '';
  var unmatched = 'Syoboi:\n\n';

  var safety = [];

  var matchem = function(sbtit,set,titles) {
    var res = set.get(sbtit);
    if(res && (res[0][0] >= 0.6)) {
      var match = lc[titles[res[0][1]]];
      var iden = (match.id_mal)? match.id_mal : match.title_ro;
      if(safety.indexOf(iden) != -1) return;
      safety.push(iden);
      matched += 'Syoboi\'s '+sb[j].title_ja+' matched to livechart\'s '+match.title_ja+' at '+res[0][0]+'\n';
      var bothSources = _.union(match.sources, sb[j].sources);
      _.extend(match,sb[j]);
      match.sources = bothSources;
      merged.push(match);
      indexes = _.reject(indexes, function(item,index) {
        if(titles[res[0][1]] == item) return true;
      });
      return true;
    } else return false;
  }

  var clean = function(title) {
    title = title.replace(/\d+/g, '')
    title = title.replace(/(\(|\[|（).+(）|\]|\))/g, ' ');
    title = title.replace(/[〈〉\(\)\]\[\!！\?？－）（＜＞（）【】《》「」『』。\.,、・゜°･♡♥～~＋+]/g, ' ');
    //title = title.replace(/[^a-zA-Z]+/)
    title = title.replace('   ', ' ');
    title = title.replace('  ', ' ');
    title = title.replace('　', ' ');
    title = title.trim();
    //console.log(title);
    return title;
  }
  var clean2 = function(title) {
    title = title.replace(/[ＲRＳSＸXＶVＱQＷWＹYＺZ]/g, '');
    title = title.split(' ')[0];
    return title;
  }
  var clean3 = function(title) {
    title = title.replace(' ', '');
    return title;
  }

  for(var i in lc) {
    var lctit = clean(lc[i].title_ja);
    //var lctit = lc[i].title_ja;
    //console.log(lctit);
    var lctitc = clean2(lctit);
    var lctitd = clean3(lctit);
    a0.add(lc[i].title_ja);
    a.add(lctit);
    b.add(lc[i].title_ro);
    c.add(lctitc);
    d.add(lctitd);
    titles0[lc[i].title_ja] = i;
    titles[lctit] = i;
    titlesb[lc[i].title_ro] = i;
    titlesc[lctitc] = i;
    titlesd[lctitd] = i;
    indexes.push(i);
  }; //console.log(titles,titlesb);
  for(var j in sb) {
    var sbtit = sb[j].title_ja;
    if(!matchem(sbtit,a0,titles0)) {
      sbtit = clean(sb[j].title_ja);
      if(!matchem(sbtit,a,titles)) {
        if(!matchem(sb[j].title_ja,b,titlesb)) {
          if(!matchem(clean2(sbtit),c,titlesc)) {
            if(!matchem(clean3(sbtit),d,titlesd)) {
              //merged.push(sb[j]);
              //console.log('Match for syoboi\'s '+sb[j].title_ja+' not found');
              //console.log(res);
              unmatched += sb[j].title_ja+'\n';
              //unmatched += res+'\n';
            }
          }
        }
      }
    };
    if(j == sb.length - 1) {
      //console.log('The following livechart sources were not matched:');
      unmatched += '\n\nLiveChart:\n\n'; //console.log(titles,titlesb,indexes);
      for(var k in indexes) {
        //merged.push(lc[indexes[k]]);
        //console.log(lc[indexes[k]].title_ja);
        unmatched += lc[indexes[k]].title_ja+' / '+lc[indexes[k]].title_ro+'\n';
      }
    }
  };
  fs.writeFileSync('./fails', unmatched);
  fs.writeFileSync('./matched', matched);
  fs.writeFileSync('./merged.json', JSON.stringify(merged,null,2));
  
  //patch('post');
  
  if(cb) cb();
  //crScrape();
}

var quarter = function(season) {
  var year = new Date().getFullYear();
  switch(season) {
    case 'winter':
      return (year+1)+'q'+1;
      break;
    case 'spring':
      return year+'q'+2;
      break;
    case 'summer':
      return year+'q'+3;
      break;
    case 'fall':
    case 'autumn':
      return year+'q'+4;
      break;
  }
}

var lcSeason = null;
var sbSeason = null;

if(process.argv[3]) {
  var argu = process.argv[3];
  var year = (argu == 'winter')? new Date().getFullYear() + 1 : new Date().getFullYear();
  var season = (argu == 'autumn')? 'fall' : argu;

  lcSeason = season+'-'+year;
  sbSeason = quarter(argu);
}

switch(process.argv[2]) {
  case 'fun':
    funScrape();
    break;
  case 'cr':
    crScrape();
    break;
  case 'lc':
    lcScrape(lcSeason);
    break;
  case 'sb':
    sbScrape(sbSeason);
    break;
  case 'merge':
    merge();
    break;
  case 'full':
    lcScrape(lcSeason, function() { sbScrape(sbSeason, merge) });
    break;
}
