# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0015_auto_20150114_2355'),
    ]

    operations = [
        migrations.AddField(
            model_name='show',
            name='dislike_count',
            field=models.PositiveIntegerField(editable=False, default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='show',
            name='like_count',
            field=models.PositiveIntegerField(editable=False, default=0),
            preserve_default=True,
        ),
    ]
