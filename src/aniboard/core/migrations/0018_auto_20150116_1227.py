# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0017_auto_20150116_1209'),
    ]

    operations = [
        migrations.AddField(
            model_name='usershow',
            name='dislike_count',
            field=models.PositiveIntegerField(editable=False, default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='usershow',
            name='like_count',
            field=models.PositiveIntegerField(editable=False, default=0),
            preserve_default=True,
        ),
    ]
