# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AiringDatetime',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('airing_datetime', models.DateTimeField()),
                ('show_source', models.ForeignKey(related_name='airing_datetimes', to='core.ShowSource')),
            ],
            options={
                'get_latest_by': 'airing_datetime',
                'ordering': ('airing_datetime',),
            },
            bases=(models.Model,),
        ),
    ]
