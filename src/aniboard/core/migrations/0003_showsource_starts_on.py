# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_airingdatetime'),
    ]

    operations = [
        migrations.AddField(
            model_name='showsource',
            name='starts_on',
            field=models.DateTimeField(blank=True, null=True),
            preserve_default=True,
        ),
    ]
