# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0008_auto_20141009_2343'),
    ]

    operations = [
        migrations.AddField(
            model_name='backend',
            name='auth_url',
            field=models.URLField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
