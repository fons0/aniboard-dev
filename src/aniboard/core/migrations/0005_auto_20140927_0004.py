# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations
import datetime


def assign_starts_on(apps, schema_editor):
    ShowSource = apps.get_model('core', 'ShowSource')
    current = datetime.date.today()
    current -= datetime.timedelta(days=current.weekday())

    for show_source in ShowSource.objects.all():
        # update all sources starts_on date
        starts_on = show_source.next_airing_datetime \
            or show_source.previous_airing_datetime

        if starts_on:
            show_source.starts_on = starts_on
            show_source.save()


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0004_auto_20140923_2314'),
    ]

    operations = [
        migrations.RunPython(assign_starts_on)
    ]
