from rest_framework import pagination, response


class AniboardPagination(pagination.PageNumberPagination):
    page_size = 15
    page_size_query_param = 'page_size'
    max_page_size = 50

    def get_paginated_response(self, data):
        return response.Response({
            'pages': self.page.paginator.num_pages,
            'page': self.page.number,
            'count': self.page.paginator.count,
            'results': data
        })
