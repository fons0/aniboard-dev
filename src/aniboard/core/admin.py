from django import shortcuts
from django.db import models as django_models
from django.conf import urls
from django.contrib import admin, messages
from modeltranslation import admin as translation_admin

from . import models, forms, importer, tasks


# Show
class YesNoFilter(admin.SimpleListFilter):
    def lookups(self, *args):
        return (
            ('yes', 'Yes'),
            ('no', 'No'),
        )

    def aggregate_queryset(self, queryset):
        return queryset

    def get_queryset_filter(self):
        return self.queryset_filter

    def queryset(self, request, queryset):
        queryset = self.aggregate_queryset(queryset)
        queryset_filter = self.get_queryset_filter()

        if self.value() == 'yes':
            queryset = queryset.filter(**queryset_filter).distinct()
        elif self.value() == 'no':
            queryset = queryset.exclude(**queryset_filter).distinct()

        return queryset


class FinishedFilter(YesNoFilter):
    title = 'finished'
    parameter_name = 'finished'
    queryset_filter = {
        'show_sources__last_aired_episode': django_models.F(
            'number_episodes'
        ),
    }


class NumberEpisodesFilter(YesNoFilter):
    title = 'has number of episodes'
    parameter_name = 'has_number_episodes'
    queryset_filter = {
        'number_episodes__isnull': False
    }


class MissingShowAPIDataFilter(YesNoFilter):
    title = 'has missing api data'
    parameter_name = 'has_missing_api_data'

    def aggregate_queryset(self, queryset):
        return queryset.annotate(
            total_api_datas=django_models.Count('api_data')
        )

    def get_queryset_filter(self):
        return {
            'total_api_datas__lt': models.Backend.objects.count()
        }


class MissingImageFilter(YesNoFilter):
    title = 'has missing image'
    parameter_name = 'has_missing_image'
    queryset_filter = {
        'cover_image': ''
    }


class ShowAPIDataInline(admin.TabularInline):
    model = models.ShowAPIData
    extra = 0


class ShowAdmin(translation_admin.TranslationAdmin):
    inlines = (ShowAPIDataInline,)
    prepopulated_fields = {'slug': ('name',)}
    list_filter = (
        'airing',
        FinishedFilter,
        NumberEpisodesFilter,
        MissingShowAPIDataFilter,
        MissingImageFilter
    )
    search_fields = ('name',)
    actions = ('mark_as_airing', 'mark_as_not_airing')
    import_shows_template = 'admin/core/import_shows.html'
    import_shows_form = forms.ImportShowsForm

    def get_urls(self):
        base_urls = super(ShowAdmin, self).get_urls()
        extra_urls = urls.patterns(
            '',
            urls.url(
                r'^import/$',
                self.admin_site.admin_view(self.import_shows),
                name='import_shows'
            ),
            urls.url(
                r'^import_from_file/$',
                self.admin_site.admin_view(self.import_shows_from_file),
                name='import_shows_from_file'
            )
        )
        return extra_urls + base_urls

    def import_shows(self, request):
        tasks.scrape_season.delay()
        messages.success(
            request, 'The importing process was started in the background'
        )

        return shortcuts.redirect('admin:core_show_changelist')

    def import_shows_from_file(self, request):
        import_form = self.import_shows_form(
            request.POST or None, request.FILES or None
        )

        if import_form.is_valid():
            import_file = import_form.cleaned_data['import_file']
            parser = importer.JSONShowsParser()

            added, updated, errors = parser.parse(import_file)

            for show in added:
                messages.success(request, 'Added {}'.format(show))

            for show in updated:
                messages.success(request, 'Updated {}'.format(show))

            for error in errors:
                messages.error(request, error)

            return shortcuts.redirect('admin:core_show_changelist')

        return shortcuts.render(
            request,
            self.import_shows_template,
            {'import_form': import_form, 'title': 'Import shows'}
        )

    def mark_as_airing(self, request, queryset):
        queryset.update(airing=True)
    mark_as_airing.short_description = 'Mark show as airing'

    def mark_as_not_airing(self, request, queryset):
        queryset.update(airing=False)
    mark_as_not_airing.short_description = 'Mark show as not airing'


# ShowSource
class DayFilter(admin.SimpleListFilter):
    title = 'airs on'
    parameter_name = 'day'

    def lookups(self, *args):
        return zip(models.AiringSchedule.DAYS, models.AiringSchedule.DAYS)

    def queryset(self, request, queryset):
        if self.value():
            queryset = queryset.filter(**{
                'schedules__{}__isnull'.format(self.value()): False,
            })

        return queryset


class PremiereFilter(YesNoFilter):
    title = 'is premiere'
    parameter_name = 'is_premiere'
    queryset_filter = {
        'is_premiere': True
    }


class SimulcastFilter(YesNoFilter):
    title = 'is simulcast'
    parameter_name = 'is_simulcast'
    queryset_filter = {
        'source__is_simulcast': True
    }


class AiringDatetimeExistsFilter(YesNoFilter):
    title = 'has datetimes'
    parameter_name = 'has_datetimes'
    queryset_filter = {
        'airing_datetimes__isnull': False
    }


class AiringScheduleInline(admin.TabularInline):
    model = models.AiringSchedule


class AiringDatetimeInline(admin.TabularInline):
    model = models.AiringDatetime


class ShowSourceAdmin(admin.ModelAdmin):
    inlines = (AiringScheduleInline, AiringDatetimeInline)
    list_filter = (
        'show__airing', DayFilter, PremiereFilter, SimulcastFilter,
        AiringDatetimeExistsFilter
    )
    search_fields = ('show__name', 'source__name')
    actions = ('create_airing_datetimes',)

    class Media:
        css = {
            'all': ('styles/admin/timezonewarning-fix.css',)
        }

    def save_related(self, request, form, formsets, change):
        super().save_related(request, form, formsets, change)

        show_source = form.instance

        if show_source.starts_on and not show_source.schedules.exists():
            # create an auto-schedule if needed
            starts_on = show_source.starts_on
            schedule = models.AiringSchedule()
            schedule[starts_on.weekday()] = starts_on.time()
            show_source.schedules.add(schedule)

        if not change:
            # if we just created it, generate airing datetimes just in case
            show_source.create_airing_datetimes(weeks=2)

    def create_airing_datetimes(self, request, queryset):
        for show_source in queryset.prefetch_related('schedules'):
            show_source.create_airing_datetimes(weeks=2)

        self.message_user(
            request,
            'The airing datetimes were created successfully',
            messages.SUCCESS
        )
    create_airing_datetimes.short_description = 'Create next airing datetimes'


# UserShow
class UserShowAdmin(admin.ModelAdmin):
    search_fields = ('show_source__show__name', 'user__username')
    list_display = (
        'user', 'show_source', 'last_episode_watched', 'rating', 'dropped'
    )


# UserTrace
class UserTraceAdmin(admin.ModelAdmin):
    search_fields = ('user__username', 'trace',)
    list_display = ('user', 'trace', 'date_time',)
    list_filter = ('trace',)


# RegistrationCode
class RegistrationCodeAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'created', 'used', 'user')
    readonly_fields = ('user', 'code', 'created', 'used')

    def add_view(self, request, form_url='', extra_context=None):
        registration_code = models.RegistrationCode.objects\
            .generate_registration_code()
        return self.response_add(request, registration_code)


# Notice
class NoticeAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'date_time', 'active')


admin.site.register(models.Show, ShowAdmin)
admin.site.register(models.Source, translation_admin.TranslationAdmin)
admin.site.register(models.ShowSource, ShowSourceAdmin)
admin.site.register(models.UserShow, UserShowAdmin)
admin.site.register(models.Backend)
admin.site.register(models.Settings)
admin.site.register(models.Stalking)
admin.site.register(models.UserTrace, UserTraceAdmin)
admin.site.register(models.RegistrationCode, RegistrationCodeAdmin)
admin.site.register(models.Notice, NoticeAdmin)
