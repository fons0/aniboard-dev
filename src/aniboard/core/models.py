from django import dispatch
from django.db import models
from django.core import exceptions
from django.utils import timezone
from django.db.models import signals
from django.contrib.auth import models as auth_models

import os
import random
import string
import datetime
import jsonfield
import model_utils

from aniboard import fields


def cover_image_path(instance, filename):
    extension = os.path.splitext(filename)[1]
    timestamp = datetime.datetime.now().strftime('%d%m%Y%H%M%S')

    return os.path.join(
        'shows', instance.name, 'cover_image{}{}'.format(timestamp, extension))


def backend_logo_path(instance, filename):
    extension = os.path.splitext(filename)[1]

    return os.path.join('backends', '{}{}'.format(instance.name, extension))


class AbstractScoreableModel(models.Model):
    NEUTRAL_WEIGHT = 0
    DISLIKE_WEIGTH = -0.5

    like_count = models.PositiveIntegerField(default=0, editable=False)
    dislike_count = models.PositiveIntegerField(default=0, editable=False)
    neutral_count = models.PositiveIntegerField(default=0, editable=False)

    class Meta:
        abstract = True

    @property
    def score_count(self):
        return self.like_count + self.neutral_count + self.dislike_count

    @property
    def score(self):
        score = None

        if self.score_count:
            score = round(
                (
                    self.like_count +
                    self.neutral_count * self.NEUTRAL_WEIGHT +
                    self.dislike_count * self.DISLIKE_WEIGTH
                ) * 100.0 / self.score_count
            )
            score = max(score, 0)

        return score


class Show(AbstractScoreableModel):
    name = models.CharField(max_length=500)
    slug = models.SlugField(max_length=100, unique=True)
    airing = models.BooleanField(default=True)
    number_episodes = models.PositiveIntegerField(
        verbose_name='number of episodes', null=True, blank=True)
    autoupdate_number_episodes = models.BooleanField(default=True)
    cover_image = models.ImageField(
        max_length=200,
        upload_to=cover_image_path,
        null=True,
        blank=True
    )
    sources = models.ManyToManyField(
        'Source', through='ShowSource', related_name='shows'
    )
    syoboi_id = models.CharField(max_length=100, blank=True, null=True)
    added_date = models.DateField(auto_now_add=True)

    class Meta:
        ordering = ('name',)

    def __str__(self):
        return self.name

    @property
    def cover_image_url(self):
        url = None

        if self.cover_image:
            url = self.cover_image.url

        return url


class ShowEpisode(AbstractScoreableModel):
    show = models.ForeignKey('Show', related_name='episodes')
    episode = models.PositiveIntegerField()

    def __str__(self):
        return '{}, Episode {}'.format(self.show, self.episode)


class Source(models.Model):
    name = models.CharField(max_length=255)
    is_simulcast = models.BooleanField(default=False)

    class Meta:
        ordering = ('name',)

    def __str__(self):
        return self.name


class ShowSource(models.Model):
    show = models.ForeignKey('Show', related_name='show_sources')
    source = models.ForeignKey('Source')
    last_aired_episode = models.PositiveIntegerField(default=0)
    starts_on = models.DateTimeField(null=True, blank=True)
    is_premiere = models.BooleanField(default=False)

    class Meta:
        ordering = ('show__name', 'source__name')

    def __str__(self):
        return '{0} - {1}'.format(self.show.name, self.source.name)

    def is_scheduleable(self):
        ''' Check if the source is scheduleable by checking there exist a start
        date and at least one non-empty schedule.
        '''
        return self.starts_on and any(self.schedules.all())

    def schedule_for(self, date):
        ''' Returns the schedule for the week of the given date. '''
        schedule = None
        schedules = list(self.schedules.all().order_by('order'))

        if schedules and self.starts_on:
            # we only need dates for this, no time
            starts_on = self.starts_on.date()

            if date >= starts_on:
                # calculate the weeks difference between the start of the show
                # and the given date
                target_week = date - datetime.timedelta(days=date.weekday())
                start_week = starts_on \
                    - datetime.timedelta(days=starts_on.weekday())
                weeks = (target_week - start_week).days // 7

                # return the necesary schedule
                schedule = schedules[weeks % len(schedules)]

        return schedule

    def create_airing_datetimes(self, offset=0, weeks=0, force_one=True):
        ''' Creates airing datetimes based on the show source shedule.
        The `offset` indicates how many eeks to move forward or backward from
        today to start creating schedules, while `weeks` indicate how many
        weeks should it look into the schedules to create new datetimes.
        Finally, `force_one` indicates that if no valid schedule is found in
        the defined range, the method should continue until at least one
        airing datetime is created. In this case, indicating 0 or negative
        `weeks` would mean to continue exploring the schedules until an airing
        datetime gets created.
        '''
        if not self.is_scheduleable():
            # if the source isn't scheduleable, quit
            return

        today = datetime.date.today()
        current = today + datetime.timedelta(days=offset * 7 - today.weekday())
        starts_on = self.starts_on.date()

        if starts_on > current:
            # respect the starts_on date
            start_week = starts_on \
                - datetime.timedelta(days=starts_on.weekday())
            weeks -= (start_week - current).days // 7
            current = starts_on

        # we have to make sure at lest ONE airing datetime gets created
        # for the "not airing this week" section to work
        created = False

        # we keep a count of how many episodes would the series have so far
        # to avoid going over that limit
        episodes = self.last_aired_episode + self.airing_datetimes.filter(
            airing_datetime__gte=timezone.now(),
            used=False,
            skip=False
        ).count()
        total_episodes = self.show.number_episodes

        while (not total_episodes or episodes < total_episodes) \
                and (weeks > 0 or (force_one and not created)):
            schedule = self.schedule_for(current)

            for i in range(current.weekday(), 7):
                # loop through the days of the schedule
                time = schedule[i]

                if time is not None:
                    # create the datetime
                    airing_datetime = datetime.datetime(
                        year=current.year,
                        month=current.month,
                        day=current.day,
                        hour=time.hour,
                        minute=time.minute,
                        tzinfo=timezone.get_default_timezone(),
                    )

                    # base query for related datetimes
                    datetimes = self.airing_datetimes.for_datetime(
                        airing_datetime,
                        use_hour=False,
                        include_used=True,
                        include_skipped=True
                    )

                    if not datetimes.exists():
                        # no datetimes exist, create a new one
                        self.airing_datetimes.create(
                            airing_datetime=airing_datetime
                        )
                        episodes += 1
                        created = True
                    elif datetimes.filter(used=False, skip=False).exists():
                        # at least one VALID datetime exists, so we can go
                        # ahead and mark it as created
                        created = True

                # next day
                current += datetime.timedelta(days=1)
            # next week
            weeks -= 1


class AiringSchedule(models.Model):
    DAYS = [
        'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday',
        'sunday'
    ]

    show_source = models.ForeignKey('ShowSource', related_name='schedules')
    order = models.PositiveSmallIntegerField(default=0)
    monday = models.TimeField(blank=True, null=True)
    tuesday = models.TimeField(blank=True, null=True)
    wednesday = models.TimeField(blank=True, null=True)
    thursday = models.TimeField(blank=True, null=True)
    friday = models.TimeField(blank=True, null=True)
    saturday = models.TimeField(blank=True, null=True)
    sunday = models.TimeField(blank=True, null=True)

    class Meta:
        ordering = ('order',)

    def __str__(self):
        return '{}:{}'.format(self.order, self.show_source)

    def __len__(self):
        return len(self.DAYS)

    def __getitem__(self, key):
        return getattr(self, self.DAYS[key])

    def __setitem__(self, key, value):
        return setattr(self, self.DAYS[key], value)

    def __iter__(self):
        for i in range(0, len(self.DAYS)):
            yield self[i]

    def __bool__(self):
        return any([time is not None for time in self])


class AiringDatetimeManager(models.Manager):
    use_for_related_fields = True

    def for_datetime(self, airing_datetime, use_hour=True, use_minute=False,
                     include_used=False, include_skipped=False):
        queryset = self.get_queryset().filter(
            airing_datetime__year=airing_datetime.year,
            airing_datetime__month=airing_datetime.month,
            airing_datetime__day=airing_datetime.day
        )

        if use_hour:
            queryset = queryset.filter(
                airing_datetime__hour=airing_datetime.hour
            )
        if use_minute:
            queryset = queryset.filter(
                airing_datetime__minute=airing_datetime.minute
            )

        if not include_used:
            queryset = queryset.filter(used=False)

        if not include_skipped:
            queryset = queryset.filter(skip=False)

        return queryset


class AiringDatetime(models.Model):
    show_source = models.ForeignKey(
        'ShowSource',
        related_name='airing_datetimes'
    )
    airing_datetime = models.DateTimeField()
    used = models.BooleanField(default=False)
    skip = models.BooleanField(default=False)

    objects = AiringDatetimeManager()

    class Meta:
        ordering = ('airing_datetime',)
        get_latest_by = 'airing_datetime'

    def __str__(self):
        return '{}: {}'.format(str(self.show_source), self.airing_datetime)


class UserShowManager(models.Manager):
    use_for_related_fields = True

    def get_queryset(self):
        return super(UserShowManager, self).get_queryset().select_related(
            'show_source__show')


class UserShow(AbstractScoreableModel):
    user = models.ForeignKey('auth.User', related_name='shows')
    show_source = models.ForeignKey('ShowSource', related_name='user_shows')
    last_episode_watched = models.PositiveIntegerField(default=0)
    dropped = models.BooleanField(default=False)
    rating = models.PositiveSmallIntegerField(null=True, blank=True)

    tracker = model_utils.FieldTracker(
        fields=('last_episode_watched', 'rating', 'dropped'))
    objects = UserShowManager()

    class Meta:
        unique_together = ('user', 'show_source')

    def __str__(self):
        return '{0}, {1}'.format(self.user, self.show_source)

    def clean(self):
        if self.last_episode_watched > self.show_source.last_aired_episode:
            raise exceptions.ValidationError(
                'This source has only aired {} episodes'.format(
                    self.show_source.last_aired_episode)
            )

    @property
    def completed(self):
        return \
            self.last_episode_watched == self.show_source.show.number_episodes


class UserShowAction(models.Model):
    user_show = models.ForeignKey('UserShow', related_name='actions')
    episode = models.PositiveIntegerField()
    action_datetime = models.DateTimeField(auto_now_add=True)
    comment = fields.LenientForeignObject(
        'UserShowComment',
        from_fields=('user_show_id', 'episode'),
        to_fields=('user_show_id', 'episode'),
        related_name='action',
        null=True,
        blank=True
    )

    class Meta:
        ordering = ('-action_datetime',)

    def __str__(self):
        return '{}, {}, {}'.format(
            self.action_datetime, self.user_show, self.episode)


class UserShowComment(models.Model):
    user_show = models.ForeignKey('UserShow', related_name='comments')
    episode = models.PositiveIntegerField()
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    liked = models.NullBooleanField()
    comment = models.CharField(max_length=140, null=True, blank=True)

    tracker = model_utils.FieldTracker(fields=('liked',))

    class Meta:
        ordering = ('-modified',)
        unique_together = ('user_show', 'episode')

    def __str__(self):
        return '{}, {}, {}'.format(
            self.modified, self.user_show, self.episode
        )


class Backend(models.Model):
    backend_name = models.CharField(max_length=255, unique=True)
    name = models.CharField(max_length=255)
    description = models.TextField()
    logo = models.ImageField(max_length=200, upload_to=backend_logo_path)
    auth_url = models.URLField(blank=True, null=True)

    def __str__(self):
        return self.name

    @property
    def logo_url(self):
        return self.logo.url


class ShowAPIData(models.Model):
    show = models.ForeignKey('Show', related_name='api_data')
    backend = models.ForeignKey('Backend')
    api_id = models.CharField(max_length=250)
    episode_offset = models.PositiveIntegerField(default=0)

    class Meta:
        unique_together = ('show', 'backend')

    def __str__(self):
        return '{} - {}'.format(self.show, self.backend)


class UserAPIData(models.Model):
    user = models.ForeignKey('auth.User', related_name='api_data')
    backend = models.ForeignKey('Backend')
    data = jsonfield.JSONField(default={})

    class Meta:
        unique_together = ('user', 'backend')

    def __str__(self):
        return '{} - {}'.format(self.user, self.backend)


class Settings(models.Model):
    user = models.OneToOneField('auth.User', related_name='settings')
    ask_rating = models.BooleanField(default=True)
    ask_comment = models.BooleanField(default=False)
    theme = models.CharField(
        max_length=100, null=True, blank=True, default='default'
    )
    last_notice_read = models.ForeignKey('Notice', null=True, blank=True)

    def __str__(self):
        return str(self.user)


class Stalking(models.Model):
    stalker = models.ForeignKey('auth.User', related_name='stalks')
    victim = models.ForeignKey('auth.User', related_name='stalked_by')

    class Meta:
        unique_together = (('stalker', 'victim'),)

    def __str__(self):
        return '{} stalks {}'.format(self.stalker, self.victim)

    def clean(self):
        if self.stalker == self.victim:
            raise exceptions.ValidationError(
                'The stalker and the victim can\'t be the same user'
            )


class UserTrace(models.Model):
    user = models.ForeignKey('auth.User', related_name='traces')
    date_time = models.DateTimeField(auto_now_add=True)
    trace = models.CharField(max_length=100, blank=True)

    class Meta:
        ordering = ('-date_time',)

    def __str__(self):
        return '{}: {}'.format(self.user, self.trace)


class RegistrationCodeManager(models.Manager):
    def generate_registration_code(self):
        return self.create(
            code=''.join(
                random.choice(string.ascii_letters) for i in range(20)
            )
        )


class RegistrationCode(models.Model):
    user = models.ForeignKey('auth.User', blank=True, null=True)
    code = models.CharField(max_length=20)
    created = models.DateTimeField(auto_now_add=True)
    used = models.DateTimeField(blank=True, null=True)

    objects = RegistrationCodeManager()

    class Meta:
        ordering = ('used', 'user',)

    def __str__(self):
        return self.code


class Notice(models.Model):
    active = models.BooleanField(default=True)
    date_time = models.DateTimeField(auto_now_add=True)
    text = models.TextField()

    class Meta:
        ordering = ('-date_time',)

    def __str__(self):
        return self.text


# Signals
@dispatch.receiver(signals.pre_save, sender=UserShow)
def mark_changed_attributes(sender=None, instance=None, **kwargs):
    instance.__last_episode_watched_changed = instance.tracker.has_changed(
        'last_episode_watched')
    instance.__rating_changed = instance.tracker.has_changed(
        'rating')
    instance.__dropped_changed = instance.tracker.has_changed('dropped')

    if instance.__last_episode_watched_changed:
        previous_watched = instance.tracker.previous('last_episode_watched')
        instance.__last_episode_watched_increased = \
            previous_watched is not None \
            and instance.last_episode_watched > previous_watched


@dispatch.receiver(signals.post_save, sender=UserShow)
def update_user_show_actions(sender=None, instance=None, **kwargs):
    if instance.__last_episode_watched_changed:
        if not instance.__last_episode_watched_increased:
            # if it decreased, we delete all the ones that are higher than the
            # current
            UserShowAction.objects.filter(
                user_show=instance, episode__gte=instance.last_episode_watched
            ).delete()

        if instance.last_episode_watched > 0:
            # if the episode is higher than 0, we record it
            UserShowAction.objects.create(
                user_show=instance, episode=instance.last_episode_watched)


@dispatch.receiver(signals.post_save, sender=UserShow)
def delete_user_show_comments(sender=None, instance=None, **kwargs):
    if instance.__last_episode_watched_changed and \
       not instance.__last_episode_watched_increased:
        # if the last episode watched was decreased, we delete all the
        # comments for following episodes
        UserShowComment.objects.filter(
            user_show=instance, episode__gt=instance.last_episode_watched
        ).delete()


@dispatch.receiver(signals.post_save, sender=UserShow)
def update_backend_show(sender=None, instance=None, created=None, **kwargs):
    from . import tasks
    method = None

    if created:
        # if created, add the show
        method = 'add_show'
    elif instance.__dropped_changed:
        # dropped attribute changed
        if instance.dropped:
            # show dropped
            if instance.last_episode_watched == 0:
                # if we haven't watched any episode, remove the show
                method = 'remove_show'
            else:
                # otherwise drop it
                method = 'drop_show'
        elif instance.last_episode_watched == 0:
            # if it get's undropped with 0 episode watched, readd it just in
            # case
            method = 'add_show'
        else:
            # otherwise it was picked up again, we just update
            method = 'push_show'
    elif instance.__last_episode_watched_changed or instance.__rating_changed:
        # if already exists and something changed, update it
        method = 'push_show'

    if method:
        tasks.create_per_backend_tasks.delay(instance.id, method)


@dispatch.receiver(signals.post_save, sender=auth_models.User)
def create_settings(
        sender=None, instance=None, created=None, raw=None, **kwargs):
    if not raw and created:
        Settings.objects.get_or_create(user=instance)


# Scores
def update_score_counts(comment, liked, neutral, disliked):
    user_show = comment.user_show
    show = user_show.show_source.show
    show_episode, _ = show.episodes.get_or_create(episode=comment.episode)

    # Show rating
    show.like_count = models.F('like_count') + liked
    show.neutral_count = models.F('neutral_count') + neutral
    show.dislike_count = models.F('dislike_count') + disliked
    show.save()

    # ShowEpisode rating
    show_episode.like_count = models.F('like_count') + liked
    show_episode.neutral_count = models.F('neutral_count') + neutral
    show_episode.dislike_count = models.F('dislike_count') + disliked
    show_episode.save()

    # UserShow rating
    user_show.like_count = models.F('like_count') + liked
    user_show.neutral_count = models.F('neutral_count') + neutral
    user_show.dislike_count = models.F('dislike_count') + disliked
    user_show.save()


@dispatch.receiver(signals.post_save, sender=UserShowComment)
def update_scores(sender=None, instance=None, created=None, **kwargs):
    if created or instance.tracker.has_changed('liked'):
        # update current count
        liked = int(instance.liked is True)
        neutral = int(instance.liked is None)
        disliked = 1 - liked - neutral

        if not created:
            # make sure to update the previous count
            previous = instance.tracker.previous('liked')
            previous_liked = int(previous is True)
            previous_neutral = int(previous is None)

            liked -= previous_liked
            neutral -= previous_neutral
            disliked -= 1 - previous_liked - previous_neutral

        update_score_counts(instance, liked, neutral, disliked)


@dispatch.receiver(signals.pre_delete, sender=UserShowComment)
def clean_scores(sender=None, instance=None, **kwargs):
    # remove the deleted comment from the score counts
    liked = -int(instance.liked is True)
    neutral = -int(instance.liked is None)
    disliked = -1 - liked - neutral

    update_score_counts(instance, liked, neutral, disliked)
