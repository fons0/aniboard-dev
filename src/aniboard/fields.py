from django.db import models


class FakeThrough(object):
    class Meta:
        auto_created = False
        app_label = 'core'
        object_name = 'Fake'

    _meta = Meta()


class LenientForeignObject(models.ForeignObject):
    def __init__(self, *args, **kwargs):
        super(LenientForeignObject, self).__init__(*args, **kwargs)
        self.rel.through = FakeThrough()

    def _check_unique_target(self):
        return []

    def get_internal_type(self):
        return 'ManyToManyField'
