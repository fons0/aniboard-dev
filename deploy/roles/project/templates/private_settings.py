# -*- coding: utf-8 -*-
DEBUG = False

SECRET_KEY = '{{ secret_key }}'

ADMINS = (
    ('Agustín Carrasco', 'asermax@gmail.com'),
    ('Tyler Trolli', 'yorioukuno@gmail.com'),
)

MANAGERS = (
    ('Agustín Carrasco', 'asermax@gmail.com'),
    ('Tyler Trolli', 'yorioukuno@gmail.com'),
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'aniboard',
        'USER': 'aniboard',
        'PASSWORD': '{{ dbpass }}',
        'HOST': '127.0.0.1',
        'PORT': '',
    }
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
        'KEY_PREFIX': 'aniboard',
    }
}

LOGGING = {
    'version': 1,
    'formatters': {
        'backends_formatter': {
            'format': '%(levelname)s %(asctime)s %(module)s %(message)s',
        }
    },
    'handlers': {
        'backends_file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': '{{ log_path }}/backends.log',
            'formatter': 'backends_formatter'
        },
    },
    'loggers': {
        'aniboard.core.backend': {
            'handlers': ['backends_file'],
            'level': 'DEBUG',
            'propagate': True,
        },
    },
}

ALLOWED_HOSTS = ('tsuiseki.moe', 'www.tsuiseki.moe')

# files
STATIC_ROOT = '{{ static_path }}'
MEDIA_ROOT = '{{ media_path }}'

# backups
DBBACKUP_BACKUP_DIRECTORY = '{{ backup_path }}'
DBBACKUP_SERVER_NAME = 'production'

# mal
MAL_USER_AGENT = '{{ mal_user_agent }}'

# anilist
ANILIST_CLIENT_ID = '{{ anilist_client_id }}'
ANILIST_CLIENT_SECRET = '{{ anilist_client_secret }}'
ANILIST_REDIRECT_URI = '{{ anilist_redirect_uri }}'

# email
EMAIL_BACKEND = 'django_ses_backend.SESBackend'
SERVER_EMAIL = '{{ email_account }}'
DEFAULT_FROM_EMAIL = '{{ email_account }}'

# aws
AWS_ACCESS_KEY_ID = '{{ aws_key }}'
AWS_SECRET_ACCESS_KEY = '{{ aws_secret }}'

# ses
AWS_SES_REGION_NAME = 'us-west-2'
AWS_SES_REGION_ENDPOINT = 'email.us-west-2.amazonaws.com'
